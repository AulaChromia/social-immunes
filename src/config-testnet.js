// eslint-disable-next-line
module.exports = {
  blockchain: {
    nodeApiUrl: "http://localhost:7740",
    rid: "07B7EA83B83FB1D24989F9B63A7896605107150F0E8BCB60C0AA3A200616A067",
    explorerBaseUrl: "https://explorer-testnet.chromia.com/"
  },
  vault: {
    url: "https://vault-testnet.chromia.com",
    callbackBaseUrl: "http://localhost:3000"
  },
  sentry: {
    dsn: "https://a45f0d3d7c5d42819cabb34e32f56998@sentry.io/1851343",
    environment: "TestNet"
  },
  matomo: {
    enabled: true,
    url: "https://matomo.chromia.dev/",
    siteId: 5,
    trackErrors: true,
    jsFileName: "js/",
    phpFilename: "js/"
  },
  topBar: {
    message: "TestNet"
  },
  features: {
    userSocialsEnabled: false
  },
  test: true,
  logLevel: "info"
};
