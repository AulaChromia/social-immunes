# This app is forked from Chromunity
[![Build Status](https://travis-ci.org/snieking/chromunity.svg?branch=master)](https://travis-ci.org/snieking/chromunity)
[![Known Vulnerabilities](https://snyk.io/test/github/snieking/chromunity/dev/badge.svg)](https://snyk.io/test/github/snieking/chromunity)

**Chromunity** is a decentralized social media site where the data is stored in a blockchain. It is built with Rell, an easy to learn and powerful language. Rell is used to interact with https://chromia.com

## Running Social Immunes

### Requirements
1. Docker
2. Node.js 12+

### Running a local Blockchain

**Social Immunes** can be run with a local blockchain which is useful during development. There are two ways to do this:

# 1

- ```cd rell```
- ```./download-binaries.sh```
- ```./build.sh``` (build.sh generates .env file and rid for you.)
- ```./run-dev-node.sh```
- ```cd to top level in directory``` then...
- ```npm install```
- ```npm run start```

Alternatively, this can be done via docker for convenience. Follow the steps below for this method.

# 2

```shell script

# A helper script is provided in the base directory for building the image.
./build-image.sh

# Blockchain can be started after that in the test directory.
cd test/docker
docker-compose up -d

# Add RID to project .env which is used for the app to correct to the correct blockchain
./add-rid-to-env.sh
```

### Starting Social Immunes
* Navigate back to project root directory
1. `npm install`
2. `npm start`

### Using the dev Vault for Single-Sign-On (SSO)

Social Immunes connects to Chromia's development Vault that can be used for local SSO during development.

1. Navigate to https://dev.vault.chromia-development.com
2. Sign-up if you don't have an account there yet. If you already have it, just sign-in.
3. Add a custom dApp, and fill in the following details:
   ```
   DApp name: Social Immunes Local
   Host: http://localhost
   Port: 7740
   Website: http://localhost:3000
   Chain ID: <take the one from your .env file>
   ```
4. Click add and you should now be able to sign-in to your local Social Immunes app using SSO.
