import React from "react";
import { Tooltip } from "@material-ui/core";
import makeStyles from "@material-ui/core/styles/makeStyles";
import WebIcon from "@material-ui/icons/Web";
import TwitterLogo from "../../common/logos/TwitterLogo";
import InstagramIcon from "@material-ui/icons/Instagram";
//import TelegramLogo from "../../common/logos/TelegramLogo";
import FacebookIcon from "@material-ui/icons/Facebook";
import GitHubLogo from "../../common/logos/GitHubLogo";
import BlockExplorerLogo from "../../common/logos/BlockExplorerLogo";

const useStyles = makeStyles((theme) => ({
  footer: {
    width: "100%",
    position: "relative",
    textAlign: "center",
    marginBottom: "10px",
  },
  link: {
    textDecoration: "none",
    color: "inherit",
    margin: "3px",
  },
  text: {
    fontSize: "12px",
    color: theme.palette.primary.main,
    fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif',
  }
}));

const Footer: React.FunctionComponent = (props) => {
  const classes = useStyles(props);

  return (
    <footer className={classes.footer}>
    <Tooltip title="Website">
      <a className={classes.link} href="http://immunes.health/" target="_blank" rel="noopener noreferrer">
        <WebIcon />
      </a>
    </Tooltip>
      <Tooltip title="Twitter">
        <a className={classes.link} href="https://twitter.com/immuneshealth" target="_blank" rel="noopener noreferrer">
          <TwitterLogo />
        </a>
      </Tooltip>
      <Tooltip title="Instagram">
        <a className={classes.link} href="https://www.instagram.com/immuneshealth/" target="_blank" rel="noopener noreferrer">
          <InstagramIcon />
        </a>
      </Tooltip>
      <Tooltip title="Facebook">
        <a className={classes.link} href="https://www.facebook.com/immuneshealth" target="_blank" rel="noopener noreferrer">
          <FacebookIcon />
        </a>
      </Tooltip>
      <Tooltip title="GitLab Repository">
        <a
          className={classes.link}
          href="https://gitlab.com/AulaChromia/social-immunes"
          target="_blank"
          rel="noopener noreferrer"
        >
          <GitHubLogo />
        </a>
      </Tooltip>
      <BlockExplorerLogo />
    </footer>
  );
};

export default Footer;
